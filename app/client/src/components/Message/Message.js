import React from 'react';
import Popover from '@material-ui/core/Popover';
import PermScanWifiIcon from '@material-ui/icons/PermScanWifi';
import AuthStyles from '../../assets/styles/AuthStyles';

const Message = (props) => {
    const { textError, openPopover, closePopoverHandler } = props;
    const classes = AuthStyles();
    return (
        <Popover
            classes={{
                paper: classes.popover,
            }}
            open={openPopover}
            anchorReference="anchorPosition"
            anchorPosition={{ top: 0, left: 0 }}
            onClose={() => closePopoverHandler()}
        >
            <h3 className={classes.popoverHeader}>
                <PermScanWifiIcon className={classes.iconMessage} />
                Message!
            </h3>
            <div className={classes.popoverBody}>{textError}</div>
        </Popover>
    );
};

export default Message;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    large: {
        width: theme.spacing(15),
        height: theme.spacing(15),
    },
    '@media screen and (max-width: 500px)': {
        large: {
            width: theme.spacing(7),
            height: theme.spacing(7),
        },
        root: {
            marginLeft: 15,
        },
    },
}));

export default function AvatarAccountPage(props) {
    const { handleClickOpen, image } = props;
    const classes = useStyles();
    // let arr = [];
    // image.map(item => arr.push(item) )

    return (
        <div className={classes.root}>
            <Avatar
                alt="Remy Sharp"
                src={image}
                className={classes.large}
                onClick={handleClickOpen}
            />
        </div>
    );
}

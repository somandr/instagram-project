import React from 'react';
import { usePost } from '../../hooks/post.hook';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import AvatarSideRecomendedFriendListStyles from '../../assets/styles/AvatarSideRecomendedFriendListStyles';
import { Button } from '@material-ui/core';
import { NavLink, useHistory } from 'react-router-dom';

export default function AvatarSideRecomendedFriendList(props) {
    const classes = AvatarSideRecomendedFriendListStyles();
    const history = useHistory();
    const { title, postedByProfilePhoto, userId, pathToProfileStories } = props;
    const { followUser } = usePost();

    const followUserHandler = () => {
        followUser(userId);
        history.push('/');
    };

    return (
        <div className={classes.root}>
            <NavLink
                to={pathToProfileStories}
                style={{ textDecoration: 'none' }}
            >
                <Box component="div" className={classes.avatarWrapper}>
                    <Avatar
                        src={postedByProfilePhoto}
                        className={classes.small}
                    />
                    <Box component="div">
                        <Typography
                            component="p"
                            variant="body2"
                            className={classes.avatarName}
                            color="textPrimary"
                        >
                            {title}
                        </Typography>
                    </Box>
                </Box>
            </NavLink>
            <Box component="div" className={classes.subscribeWrapper}>
                <Button
                    style={{ fontSize: 10 }}
                    className={classes.subscribeText}
                    onClick={followUserHandler}
                >
                    Подписаться
                </Button>
            </Box>
        </div>
    );
}

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '36ch',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));

export default function AvatarAccountDialog(props) {
    const classes = useStyles();
    const { userName, profilePhoto } = props;

    return (
        <List className={classes.root}>
            <ListItem alignItems="flex-start">
                <ListItemAvatar>
                    <Avatar src={profilePhoto} alt="Remy Sharp" />
                </ListItemAvatar>

                <ListItemText>
                    <Typography
                        component="div"
                        variant="body2"
                        className={classes.inline}
                        color="textPrimary"
                    >
                        {userName}
                    </Typography>
                </ListItemText>
            </ListItem>
        </List>
    );
}

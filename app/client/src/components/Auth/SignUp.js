import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

import { Grid, Box } from '@material-ui/core';

import Input from '../../components/Input';
import ButtonMain from '../../components/ButtonMain';
import SignUpStyles from '../../assets/styles/SignupStyles';
import appStoreBadge from '../../assets/img/signIn/appStore_badge.png';
import googlePlayBadge from '../../assets/img/signIn/googlePlay_badge.png';

export const SignUp = (props) => {
    const [form, setForm] = useState({
        email: '',
        name: '',
        username: '',
        password: '',
    });

    const [btn, setBtn] = useState(true);

    const { signUpHandler } = props;

    const changeHandler = (event) => {
        if (event.target.value.length >= 6) {
            setBtn(false);
        } else {
            setBtn(true);
        }

        setForm({
            ...form,
            [event.target.id]: event.target.value,
        });
    };

    const inputOptions = {
        onChange: changeHandler,
    };

    const inputs = [
        {
            id: 'email',
            autoComplete: 'email',
            type: 'email',
            placeholder: 'Моб. телефон или эл. адрес',
        },
        {
            id: 'name',
            autoComplete: 'name',
            type: 'text',
            placeholder: 'Имя и фамилия',
        },
        {
            id: 'username',
            autoComplete: 'name',
            type: 'text',
            placeholder: 'Имя пользователя',
        },
        {
            id: 'password',
            autoComplete: 'email',
            type: 'password',
            placeholder: 'Пароль',
        },
    ];

    const renderInputs = inputs.map((input, index) => {
        return (
            <Input
                key={index}
                id={input.id}
                autoComplete={input.autoComplete}
                type={input.type}
                placeholder={input.placeholder}
                onChange={inputOptions.onChange}
            />
        );
    });

    const classes = SignUpStyles();

    return (
        <Box component="section" className={classes.signInSection}>
            <Grid
                container
                justify="center"
                alignItems="center"
                component="main"
                className={classes.signInContainer}
            >
                <Grid item className={classes.signInFormWrapper}>
                    <Box
                        component="form"
                        className={classes.signInForm}
                        method="post"
                    >
                        <Box
                            component="div"
                            className={classes.instaLogoFormWrapper}
                        >
                            <Box
                                component="span"
                                className={classes.instaLogoForm}
                            />
                        </Box>

                        <Box className={classes.inputsWrapper}>
                            <Box component="p" className={classes.titleSignUp}>
                                Зарегистрируйтесь, чтобы смотреть фото и видео
                                ваших друзей.
                            </Box>
                            <ButtonMain>
                                <Box
                                    component="span"
                                    className={classes.fbIcon}
                                />
                                Войти через Facebook
                            </ButtonMain>
                            <Box component="span" className={classes.devider}>
                                или
                            </Box>
                            {renderInputs}
                            <ButtonMain
                                onClick={() => signUpHandler(form)}
                                disabled={btn}
                            >
                                Регистрация
                            </ButtonMain>

                            <Box
                                component="span"
                                className={classes.enterFacebook}
                            >
                                Регистрируясь, вы принимаете наши Условия,
                                Политику использования данных и Политику в
                                отношении файлов cookie.
                            </Box>
                        </Box>
                    </Box>
                    <Box className={classes.signUp}>
                        <Box component="span">
                            Есть аккаунт?
                            <NavLink to="/login">Вход</NavLink>
                        </Box>
                    </Box>
                    <Box component="p" className={classes.loadAdd}>
                        Установите приложение.
                    </Box>
                    <Box className={classes.adds}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.appstoreAdd}
                        >
                            <img src={appStoreBadge} alt="Appstore" />
                        </Box>
                        <Box
                            component="a"
                            href="#"
                            className={classes.androidAdd}
                        >
                            <img src={googlePlayBadge} alt="Google Play" />
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    );
};

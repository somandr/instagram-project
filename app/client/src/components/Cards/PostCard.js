import React, { useState } from 'react';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Box from '@material-ui/core/Box';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import PostCardStyles from '../../assets/styles/PostCardStyle';
import { NavLink } from 'react-router-dom';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';

import 'emoji-mart/css/emoji-mart.css';
import { Picker } from 'emoji-mart';

function PostCard(props) {
    const classes = PostCardStyles();

    const [saved, setSaved] = useState(false);
    const [comment, setComment] = useState(false);
    const [emojiPickerState, setEmojiPicker] = useState(false);
    const [message, setMessage] = useState('');
    const [isFirstComment, setIsFirstComment] = useState(true);

    const { postCardApi } = props;

    const {
        cardImage,
        cardTitle,
        favoritesPost,
        toggleLikes,
        deletePostHandler,
        makeCommentHandler,
        userPostedById,
        userId,
        postId,
        postComments,
        pathToProfile,
        cardAvatar,
        postLikes,
        postDate,
    } = postCardApi;

    const [favorites, setFavorites] = useState(favoritesPost);

    const handleFavoritesClick = (postIdent) => {
        setFavorites(!favorites);
        toggleLikes(postIdent);
    };
    const handleSaveClick = () => {
        setSaved(!saved);
    };
    const handleCommentClick = () => {
        setComment(!comment);
    };

    function triggerPicker(event) {
        event.preventDefault();
        setEmojiPicker(!emojiPickerState);
    }

    const deleteBtn =
        userId === userPostedById ? (
            <DeleteOutlinedIcon
                className={classes.deleteIcon}
                onClick={() => deletePostHandler(postId)}
            />
        ) : (
            <IconButton aria-label="settings">
                <MoreVertIcon />
            </IconButton>
        );

    const firstComment = !postComments[postComments.length - 1]
        ? false
        : postComments[postComments.length - 1];

    const textFirstComment = firstComment ? firstComment.text : null;
    const postedByFirstComment = firstComment.postedBy
        ? firstComment.postedBy.username
        : null;
    const renderPostComments = isFirstComment ? (
        <Box style={{ display: 'flex' }}>
            <Typography
                variant="body1"
                color="textPrimary"
                style={{
                    fontWeight: 500,
                    paddingRight: 10,
                }}
            >
                {postedByFirstComment}
            </Typography>
            <Typography variant="body1" color="textPrimary">
                {textFirstComment}
            </Typography>
        </Box>
    ) : (
        postComments.map((commentItem) => {
            const commentsByName = !commentItem.postedBy
                ? null
                : commentItem.postedBy.username;
            return (
                <Box style={{ display: 'flex' }}>
                    <Typography
                        variant="body1"
                        color="textPrimary"
                        style={{
                            fontWeight: 500,
                            paddingRight: 10,
                        }}
                    >
                        {commentsByName}
                    </Typography>
                    <Typography variant="body1" color="textPrimary">
                        {commentItem.text}
                    </Typography>
                </Box>
            );
        })
    );
    return (
        <Card className={classes.root}>
            <CardHeader
                avatar={
                    <NavLink to={pathToProfile}>
                        <Avatar
                            aria-label="recipe"
                            alt="Profile Photo"
                            src={cardAvatar}
                            className={classes.avatar}
                        />
                    </NavLink>
                }
                action={<>{deleteBtn}</>}
                title={
                    <NavLink to={pathToProfile} className={classes.avatarTitle}>
                        {cardTitle}
                    </NavLink>
                }
            />
            <CardMedia onDoubleClick={() => handleFavoritesClick(postId)}>
                <Box
                    component="img"
                    src={cardImage}
                    className={classes.cardMediaContent}
                    alt="Post Image"
                />
            </CardMedia>

            <CardActions disableSpacing className={classes.cardActionWrapper}>
                <Box component="div">
                    <IconButton
                        onClick={() => handleFavoritesClick(postId)}
                        aria-expanded={favorites}
                        aria-label="add to favorites"
                    >
                        <Box
                            component="div"
                            className={classes.instaFavoriteIconWrapper}
                        >
                            <Box
                                component="span"
                                className={clsx(
                                    classes.instaFavoriteIconFalse,
                                    {
                                        [classes.instaFavoriteIconTrue]: favorites,
                                    }
                                )}
                            />
                        </Box>
                    </IconButton>
                    <IconButton
                        aria-label="comment"
                        aria-expanded={comment}
                        onClick={handleCommentClick}
                    >
                        <Box
                            component="span"
                            className={classes.instaCommentIcon}
                        />
                    </IconButton>
                    <IconButton aria-label="send-message">
                        <Box
                            component="span"
                            className={classes.instaSendMessage}
                        />
                    </IconButton>
                </Box>
                <Box component="div">
                    <IconButton
                        aria-label="save"
                        onClick={handleSaveClick}
                        aria-expanded={saved}
                    >
                        <Box
                            component="div"
                            className={classes.instaSaveIconWrapper}
                        >
                            <Box
                                component="span"
                                className={clsx(classes.instaSaveIconFalse, {
                                    [classes.instaSaveIconTrue]: saved,
                                })}
                            />
                        </Box>
                    </IconButton>
                </Box>
            </CardActions>
            <CardContent>
                <Typography variant="body2" color="textPrimary" component="p">
                    {postLikes} Likes
                </Typography>
            </CardContent>
            <CardContent>
                <Box className={classes.commentsWrap}>{renderPostComments}</Box>
                {postComments.length > 1 && (
                    <Button
                        className={classes.btnMoreComments}
                        onClick={() => setIsFirstComment(!isFirstComment)}
                    >
                        Посмотреть все комментарии ({postComments.length})
                    </Button>
                )}
            </CardContent>
            <CardContent>
                <Typography variant="body2" color="textPrimary" component="p">
                    {postDate}
                </Typography>
            </CardContent>
            <form
                noValidate
                onSubmit={(e) => {
                    e.preventDefault();
                    makeCommentHandler(e.target[0].value, postId);
                    setEmojiPicker(false);
                    setMessage('');
                }}
                className={clsx(classes.instaCommentFalse, {
                    [classes.instaCommentTrue]: comment,
                })}
            >
                <TextField
                    placeholder="Добавьте комментарий..."
                    multiline
                    fullWidth
                    onChange={(event) => setMessage(event.target.value)}
                    value={message}
                />
                <EmojiEmotionsIcon
                    onClick={(e) => triggerPicker(e)}
                    style={{ color: 'grey', marginLeft: 10 }}
                />
                <Button
                    color="primary"
                    onClick={handleCommentClick}
                    className={classes.publishButton}
                    type="submit"
                >
                    Опубликовать
                </Button>
            </form>
            <div className="emoji-table">
                {emojiPickerState && (
                    <Picker
                        title="Pick your emoji…"
                        emoji="point_up"
                        onSelect={(emoji) => setMessage(message + emoji.native)}
                        style={{ width: '100%' }}
                    />
                )}
            </div>
        </Card>
    );
}

export default PostCard;

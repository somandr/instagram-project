import React from 'react';
// used for making the prop types of this component

// core components
import Button from './../CustomButtons/Button';

import defaultImage from '../../assets/img/image_placeholder.jpg';
import defaultAvatar from '../../assets/img/placeholder.jpg';

class ImageUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            file: null,
            imagePreviewUrl: this.props.avatar ? defaultAvatar : defaultImage,
        };
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
    }

    handleImageChange(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            this.setState({
                file,
                imagePreviewUrl: reader.result,
            });
        };
        reader.readAsDataURL(file);
    }

    handleSubmit(e) {
        e.preventDefault();
    }

    handleClick() {
        this.refs.fileInput.click();
    }

    handleRemove() {
        this.setState({
            file: null,
            imagePreviewUrl: this.props.avatar ? defaultAvatar : defaultImage,
        });
        this.refs.fileInput.value = null;
    }
    render() {
        let {
            avatar,
            addButtonProps,
            removeButtonProps,
            imageData,
        } = this.props;

        imageData(this.state.file);

        return (
            <div className="fileinput text-center">
                <input
                    style={{ display: 'none' }}
                    type="file"
                    onChange={this.handleImageChange}
                    ref="fileInput"
                />
                <div className={'thumbnail' + (avatar ? ' img-circle' : '')}>
                    <img
                        src={this.state.imagePreviewUrl}
                        style={{ width: '100%' }}
                        alt="..."
                    />
                </div>
                <div>
                    {this.state.file === null ? (
                        <Button
                            {...addButtonProps}
                            onClick={() => this.handleClick()}
                        >
                            {avatar ? 'Add Photo' : 'Выбрать фото'}
                        </Button>
                    ) : (
                        <span>
                            {avatar ? <br /> : null}
                            <Button
                                {...removeButtonProps}
                                onClick={() => this.handleRemove()}
                            >
                                <i className="fas fa-times" /> Удалить
                            </Button>
                        </span>
                    )}
                </div>
            </div>
        );
    }
}

export default ImageUpload;

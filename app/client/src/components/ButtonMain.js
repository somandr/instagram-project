import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const ButtonMainStyled = withStyles({
    root: {
        // height: '30px',

        transition: 'none',
        width: '100%',
        backgroundColor: '#008CFB',
        duration: 'none',
        '&$disabled': {
            color: '#fff',
        },
        '&:hover': {
            opacity: '1',
            backgroundColor: '#008CFB',
        },
    },
    disabled: {
        opacity: '.3',
    },
    text: {
        color: '#fff',
        textTransform: 'none',
        fontSize: '14.5px',
        fontWeight: 600,
        padding: '2px',
    },

    // '& .MuiButton-root:hover': {
    //     backgroundColor: '#5C94F3',
    // },
})(Button);

const ButtonMain = (props) => <ButtonMainStyled {...props} />;

export default ButtonMain;

import React, { useState, useRef } from 'react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { Box, Avatar } from '@material-ui/core';
import { NavLink } from 'react-router-dom';

export default function MenuListComposition(props) {
    const { handleToggle, searchResults, userId, clearSearchInput } = props;
    const [open, setOpen] = useState(handleToggle);
    const anchorRef = useRef(null);

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        clearSearchInput();
        setOpen(false);
    };

    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen(false);
        }
    }

    const renderSearchResults = searchResults.map((item, index) => {
        const pathToProfile =
            item._id !== userId ? `/profile/${item._id}` : '/profile/user';

        const photoJpeg = item.photos[0].includes('jpeg')
            ? item.photos[0]
            : item.photos[1];

        return (
            <MenuItem
                key={index}
                onClick={handleClose}
                style={{
                    borderBottom: '1px solid #F0F0F0',
                }}
            >
                <NavLink
                    to={pathToProfile}
                    style={{
                        display: 'flex',
                        height: 40,
                        padding: '8px 2px',
                    }}
                >
                    <Box>
                        <Avatar
                            aria-label="recipe"
                            alt="Profile Photo"
                            src={photoJpeg}
                            style={{
                                width: 38,
                                height: 38,
                                margin: '0px 8px 0 0',
                            }}
                        />
                    </Box>
                    <Box
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                        }}
                    >
                        <span
                            style={{
                                marginBottom: 0,
                                overflow: 'hidden',
                                fontWeight: 500,
                                textOverflow: 'ellipsis',
                                fontSize: 14,
                            }}
                        >
                            {item.username}
                        </span>
                        <span
                            style={{
                                fontSize: '14px',
                                fontWeight: 400,
                                overflow: 'hidden',
                                textAlign: 'left',
                                textOverflow: 'ellipsis',
                                color: '#8E8E8E',
                            }}
                        >
                            {item.name}
                        </span>
                    </Box>
                </NavLink>
            </MenuItem>
        );
    });

    return (
        <div>
            <span ref={anchorRef} />
            <Popper
                open={open}
                anchorEl={anchorRef.current}
                role={undefined}
                transition
                disablePortal
            >
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin:
                                placement === 'bottom'
                                    ? 'center top'
                                    : 'center bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList
                                    id="menu-list-grow"
                                    onKeyDown={handleListKeyDown}
                                    style={{
                                        maxHeight: 364,
                                        overflow: 'auto',
                                        whiteSpace: 'nowrap',
                                        padding: 0,
                                    }}
                                >
                                    {renderSearchResults}
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </div>
    );
}

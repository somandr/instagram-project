import React, { forwardRef } from 'react';
import { Dialog, Slide, MenuItem, MenuList, Box } from '@material-ui/core';

const Transition = forwardRef((props, ref) => (
    <Slide direction="down" {...props} ref={ref} />
));

const Modal = (props) => {
    const { openModal, modalToggleHandler, logout } = props;

    const menuLinks = [
        {
            link: 'Выйти',
            onClick: logout,
        },
        {
            link: 'Отмена',
            onClick: modalToggleHandler,
        },
    ];
    const renderMenuLinks = menuLinks.map((item, index) => {
        return (
            <MenuItem
                key={index}
                onClick={item.onClick}
                style={{
                    borderBottom: '1px solid #F0F0F0',
                    height: 48,
                    width: 400,
                    display: 'flex',
                    alignItems: ' center',
                    justifyContent: 'center',
                }}
            >
                <Box
                    style={{
                        display: 'flex',
                        height: 48,
                        padding: '8px 4px',
                        textDecoration: 'none',
                        alignItems: ' center',
                        justifyContent: 'center',
                    }}
                >
                    <span
                        style={{
                            fontSize: 14,
                            color: '#262626',
                        }}
                    >
                        {item.link}
                    </span>
                </Box>
            </MenuItem>
        );
    });

    return (
        <Dialog
            disableBackdropClick={false}
            disableEscapeKeyDown={false}
            open={openModal}
            onClose={modalToggleHandler}
            TransitionComponent={Transition}
            maxWidth="sm"
            width={400}
        >
            <MenuList
                id="menu-list-grow"
                style={{
                    maxHeight: 364,
                    overflow: 'auto',
                    whiteSpace: 'nowrap',
                    padding: 0,
                }}
            >
                {renderMenuLinks}
            </MenuList>
        </Dialog>
    );
};

export default Modal;

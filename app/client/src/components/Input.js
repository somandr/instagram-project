import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import { Input } from '@material-ui/core';

const InputStyled = withStyles({
    input: {
        backgroundColor: '#F6F6F6',
        border: '1px solid #DBDBDB',
        borderRadius: '3px',
        fontSize: '12px',
        height: '25px',
        padding: '7px 0px 2px 8px',
        '&:focus': {
            border: '1px solid #BDBDBD',
        },
        '&::placeholder': {
            color: 'black',
            fontSize: '12px',
            textDecoration: 'none',
        },
    },
})(Input);

const InputForm = (props) => {
    return (
        <InputStyled
            fullWidth={true}
            variant="filled"
            disableUnderline={true}
            autoFocus={false}
            {...props}
        />
    );
};

export default InputForm;

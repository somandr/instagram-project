import React, { useState, useContext, forwardRef } from 'react';
import { NavLink, useHistory } from 'react-router-dom';

import { AuthContext } from '../../context/AuthContext';
import { UserContext } from '../../context/UserContext';
import { useHttp } from '../../hooks/http.hook';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Slide from '@material-ui/core/Slide';

import InputCustom from '../../components/Input';
import NavBarStyles from '../../assets/styles/components/NavBar/NavBarStyles';
import instaLogoAppBar from '../../assets/img/logo/instaLogoAppBar.png';
import ImageUpload from '../CustomUpload/ImageUpload';
import Popover from './../Popover/Popover';

import {
    withStyles,
    Box,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Button,
    Avatar,
} from '@material-ui/core';

const InputStyled = withStyles({
    input: {
        backgroundColor: '#F6F6F6',
        border: '1px solid #DBDBDB',
        borderRadius: '3px',
        fontSize: '12px',
        height: '17px',
        padding: '7px 0px 2px 8px',
        '&:focus': {
            border: '1px solid #BDBDBD',
        },
        '&::placeholder': {
            color: '#000000',
            fontSize: '14px',
            textDecoration: 'none',
            textAlign: 'center',
        },
    },
})(InputCustom);

const AppBarStyled = withStyles({
    root: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '54px',
        backgroundColor: '#FFFFFF',
        color: 'green',
        boxShadow: 'none',
        borderBottom: '1px solid #DBDBDB',
    },
})(AppBar);

const ToolbarStyled = withStyles({
    root: {
        justifyContent: 'space-between',
        width: 'calc(66% - 1px)',
        heiht: '40px',
        backgroundColor: '#FFFFFF',
        color: 'green',
        boxShadow: 'none',
        borderBottom: '1px solid #DBDBDB',
    },
    regular: {
        minHeight: '100%',
    },
})(Toolbar);

const Transition = forwardRef((props, ref) => (
    <Slide direction="down" {...props} ref={ref} />
));

const NavBar = () => {
    const { userId, token } = useContext(AuthContext);
    const { userInfo } = useContext(UserContext);
    const { request } = useHttp();
    const [open, setOpen] = useState(false);
    const [image, setImage] = useState('');
    const [searchUser, setSaerchUser] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const classes = NavBarStyles();
    const history = useHistory();
    let userInfoPhotos;

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleToggle = !!searchUser;
    const imageData = (imageValue) => {
        setImage(imageValue);
    };

    const postData = (e) => {
        e.preventDefault();
        const file = image;
        const formData = new FormData();
        formData.append('img', file);
        formData.append('user', userId);
        formData.append('filename', file.name);
        fetch('/api/authorized/post/create', {
            method: 'POST',
            body: formData,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        }).then(() => {
            alert('Публикация размещена, успешно!!!');
            handleClose();
            history.push('/');
        });
    };

    if (userInfo) {
        userInfoPhotos = userInfo.photos[0].includes('jpeg')
            ? userInfo.photos[0]
            : userInfo.photos[1];
    }

    const searchUsers = async (query) => {
        try {
            setSaerchUser(query);
            const fetched = await request(
                '/api/authorized/user/search',
                'POST',
                {
                    query,
                },
                {
                    Authorization: `Bearer ${token}`,
                }
            );
            setSearchResults(fetched.user);
        } catch (error) {}
    };

    const clearSearchInput = () => {
        document.getElementById('inputSearch').value = '';
    };
    return (
        <>
            <AppBarStyled>
                <ToolbarStyled>
                    <Box className={classes.logoWrap}>
                        <NavLink to="/">
                            <img src={instaLogoAppBar} alt="Logo Instagram" />
                        </NavLink>
                    </Box>
                    <Box className={classes.searchWrap} id="search">
                        <InputStyled
                            className={classes.inputSearch}
                            id="inputSearch"
                            fullWidth={true}
                            variant="filled"
                            disableUnderline={true}
                            autoFocus={false}
                            placeholder="Поиск"
                            onChange={(e) => searchUsers(e.target.value)}
                        />
                        <Box className={classes.popper}>
                            {searchUser && (
                                <Popover
                                    handleToggle={handleToggle}
                                    searchResults={searchResults}
                                    userId={userId}
                                    clearSearchInput={clearSearchInput}
                                />
                            )}
                        </Box>
                    </Box>
                    <Box className={classes.linksAppBarWrap}>
                        <NavLink
                            to="/posts"
                            exact
                            activeClassName={classes.activeLink}
                            className={classes.iconsAppBar}
                        >
                            <svg
                                aria-label="Home"
                                fill="#262626"
                                height="22"
                                viewBox="0 0 48 48"
                                width="22"
                            >
                                <path d="M45.5 48H30.1c-.8 0-1.5-.7-1.5-1.5V34.2c0-2.6-2.1-4.6-4.6-4.6s-4.6 2.1-4.6 4.6v12.3c0 .8-.7 1.5-1.5 1.5H2.5c-.8 0-1.5-.7-1.5-1.5V23c0-.4.2-.8.4-1.1L22.9.4c.6-.6 1.6-.6 2.1 0l21.5 21.5c.3.3.4.7.4 1.1v23.5c.1.8-.6 1.5-1.4 1.5z" />
                            </svg>
                        </NavLink>
                        <NavLink
                            exact
                            to="/direct"
                            activeClassName={classes.activeLink}
                            className={classes.iconsAppBar}
                        >
                            <svg
                                fill="#262626"
                                height="22"
                                viewBox="0 0 48 48"
                                width="22"
                            >
                                <path d="M47.8 3.8c-.3-.5-.8-.8-1.3-.8h-45C.9 3.1.3 3.5.1 4S0 5.2.4 5.7l15.9 15.6 5.5 22.6c.1.6.6 1 1.2 1.1h.2c.5 0 1-.3 1.3-.7l23.2-39c.4-.4.4-1 .1-1.5zM5.2 6.1h35.5L18 18.7 5.2 6.1zm18.7 33.6l-4.4-18.4L42.4 8.6 23.9 39.7z" />
                            </svg>
                        </NavLink>
                        <NavLink
                            to="/"
                            exact
                            activeClassName={classes.activeLink}
                            className={classes.iconsAppBar}
                            onClick={handleClickOpen}
                        >
                            <svg
                                aria-label="Find People"
                                fill="#262626"
                                height="22"
                                viewBox="0 0 48 48"
                                width="22"
                            >
                                <path d="M24 0C10.8 0 0 10.8 0 24s10.8 24 24 24 24-10.8 24-24S37.2 0 24 0zm0 45C12.4 45 3 35.6 3 24S12.4 3 24 3s21 9.4 21 21-9.4 21-21 21zm10.2-33.2l-14.8 7c-.3.1-.6.4-.7.7l-7 14.8c-.3.6-.2 1.3.3 1.7.3.3.7.4 1.1.4.2 0 .4 0 .6-.1l14.8-7c.3-.1.6-.4.7-.7l7-14.8c.3-.6.2-1.3-.3-1.7-.4-.5-1.1-.6-1.7-.3zm-7.4 15l-5.5-5.5 10.5-5-5 10.5z" />
                            </svg>
                        </NavLink>
                        <NavLink
                            to="/"
                            exact
                            activeClassName={classes.activeLink}
                            className={classes.iconsAppBar}
                        >
                            <svg
                                aria-label="Activity Feed"
                                fill="#262626"
                                height="22"
                                viewBox="0 0 48 48"
                                width="22"
                            >
                                <path d="M34.6 6.1c5.7 0 10.4 5.2 10.4 11.5 0 6.8-5.9 11-11.5 16S25 41.3 24 41.9c-1.1-.7-4.7-4-9.5-8.3-5.7-5-11.5-9.2-11.5-16C3 11.3 7.7 6.1 13.4 6.1c4.2 0 6.5 2 8.1 4.3 1.9 2.6 2.2 3.9 2.5 3.9.3 0 .6-1.3 2.5-3.9 1.6-2.3 3.9-4.3 8.1-4.3m0-3c-4.5 0-7.9 1.8-10.6 5.6-2.7-3.7-6.1-5.5-10.6-5.5C6 3.1 0 9.6 0 17.6c0 7.3 5.4 12 10.6 16.5.6.5 1.3 1.1 1.9 1.7l2.3 2c4.4 3.9 6.6 5.9 7.6 6.5.5.3 1.1.5 1.6.5.6 0 1.1-.2 1.6-.5 1-.6 2.8-2.2 7.8-6.8l2-1.8c.7-.6 1.3-1.2 2-1.7C42.7 29.6 48 25 48 17.6c0-8-6-14.5-13.4-14.5z" />
                            </svg>
                        </NavLink>
                        <NavLink
                            exact
                            to="/profile/user"
                            activeClassName="activeLink"
                        >
                            <Avatar
                                aria-label="recipe"
                                alt="Profile Photo"
                                src={userInfoPhotos}
                                style={{ width: 23, height: 23 }}
                            />
                        </NavLink>
                    </Box>
                </ToolbarStyled>
            </AppBarStyled>
            <Dialog
                className={classes.modal}
                disableBackdropClick={false}
                disableEscapeKeyDown={false}
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
                fullWidth
                maxWidth="sm"
            >
                <DialogTitle>Создать пост</DialogTitle>
                <DialogContent>
                    <h4>Создайте пост</h4>
                    <Box className={classes.dialogContentWrap}>
                        <ImageUpload
                            addButtonProps={{ round: true }}
                            changeButtonProps={{ round: true }}
                            removeButtonProps={{
                                round: true,
                                color: 'info',
                            }}
                            imageData={imageData}
                        />
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Отмена
                    </Button>
                    <Button onClick={postData} color="primary">
                        Опубликовать
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default NavBar;

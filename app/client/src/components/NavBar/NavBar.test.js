import React from 'react';
import NavBar from './NavBar';
import { act } from 'react-dom/test-utils';
import { render, unmountComponentAtNode } from 'react-dom';

let container = null;
beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
});

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

describe('Testing NavBar', () => {
    test('Navbar are rendered successfully', () => {
        act(() => {
            render(<NavBar />, container);
        });
    });
});

import { useState, useCallback, useContext } from 'react';
import { useHttp } from './http.hook';
import { AuthContext } from '../context/AuthContext';

export const usePost = () => {
    const { request } = useHttp();
    const { token } = useContext(AuthContext);
    const [newPosts, setNewPosts] = useState([]);
    const [newUserData, setNewUserData] = useState(null);

    const likePost = useCallback(
        async (id, posts) => {
            try {
                const fetched = await request(
                    '/api/authorized/post/like',
                    'PUT',
                    {
                        postId: id,
                    },
                    {
                        Authorization: `Bearer ${token}`,
                    }
                );
                const newUserPosts = posts.map((item) => {
                    if (item._id === fetched._id) {
                        return fetched;
                    } else {
                        return item;
                    }
                });
                setNewPosts(newUserPosts);
            } catch (e) {}
        },
        [request, token]
    );

    const unlikePost = useCallback(
        async (id, posts) => {
            try {
                const fetched = await request(
                    '/api/authorized/post/unlike',
                    'PUT',
                    {
                        postId: id,
                    },
                    {
                        Authorization: `Bearer ${token}`,
                    }
                );
                const newUserPosts = posts.map((item) => {
                    if (item._id === fetched._id) {
                        return fetched;
                    } else {
                        return item;
                    }
                });
                setNewPosts(newUserPosts);
            } catch (e) {}
        },
        [request, token]
    );

    const makeComment = useCallback(
        async (text, postId, posts) => {
            try {
                const fetched = await request(
                    '/api/authorized/post/comment',
                    'PUT',
                    {
                        postId,
                        text,
                    },
                    {
                        Authorization: `Bearer ${token}`,
                    }
                );
                const newUserPosts = posts.map((item) => {
                    if (item._id === fetched._id) {
                        return fetched;
                    } else {
                        return item;
                    }
                });
                setNewPosts(newUserPosts);
            } catch (e) {}
        },
        [request, token]
    );

    const deletePost = useCallback(
        async (postid, posts) => {
            try {
                const fetched = await request(
                    `/api/authorized/post/delete/${postid}`,
                    'DELETE',
                    null,
                    {
                        Authorization: `Bearer ${token}`,
                    }
                );
                const newUserPosts = posts.filter((item) => {
                    return item._id !== fetched._id;
                });
                setNewPosts(newUserPosts);
            } catch (e) {}
        },
        [request, token]
    );

    const followUser = useCallback(
        async (qparamsId) => {
            try {
                const fetched = await request(
                    '/api/authorized/user/follow',
                    'POST',
                    {
                        followId: qparamsId,
                    },
                    {
                        Authorization: `Bearer ${token}`,
                    }
                );
                setNewUserData(fetched.userFollow);
            } catch (e) {}
        },
        [request, token]
    );

    const unfollowUser = useCallback(
        async (qparamsId) => {
            try {
                const fetched = await request(
                    '/api/authorized/user/unfollow',
                    'POST',
                    {
                        unfollowId: qparamsId,
                    },
                    {
                        Authorization: `Bearer ${token}`,
                    }
                );
                setNewUserData(fetched.userFollow);
            } catch (e) {}
        },
        [request, token]
    );

    const ISOtoLongDate = useCallback((isoString, locale = 'en-US') => {
        const options = {
            month: 'long',
            day: 'numeric',
            year: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
        };
        const date = new Date(isoString);
        const longDate = new Intl.DateTimeFormat(locale, options).format(date);
        return longDate;
    }, []);

    return {
        likePost,
        unlikePost,
        followUser,
        unfollowUser,
        makeComment,
        deletePost,
        ISOtoLongDate,
        newPosts,
        newUserData,
    };
};

import { useState, useEffect, useCallback } from 'react';

export const useInfiniteScroll = (callback) => {
    const [isFetching, setIsFetching] = useState(false);

    const handleScroll = useCallback(() => {
        // eslint-disable-next-line curly
        if (
            window.innerHeight + document.documentElement.scrollTop !==
                document.documentElement.offsetHeight ||
            isFetching
        )
            return;

        setIsFetching(true);
    }, [isFetching]);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [handleScroll]);

    useEffect(() => {
        // eslint-disable-next-line curly
        if (!isFetching) return;

        callback(() => {
            console.log('called back');
        });
    }, [isFetching, callback]);

    return [isFetching, setIsFetching];
};

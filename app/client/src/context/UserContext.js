import { createContext } from 'react';

function noop() {}

export const UserContext = createContext({
    userInfo: null,
    setUserInfo: noop,
});

import React, { useState } from 'react';

import Box from '@material-ui/core/Box';
import CardMedia from '@material-ui/core/CardMedia';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';

import MuiDialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import Typography from '@material-ui/core/Typography';
import MuiDialogContent from '@material-ui/core/DialogContent/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

// import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog/Dialog';
import AvatarAccountDialog from '../../components/Avatar/Avatar-account-dialog';

import CardActions from '@material-ui/core/CardActions/CardActions';
import CardContent from '@material-ui/core/CardContent';
// import TextField from '@material-ui/core/TextField/TextField';
// import { Picker } from 'emoji-mart';
import AccountPublicationStyle from '../../assets/styles/AccountPublicationStyle';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(0.5),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});
const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(0.5),
        width: '320px',
    },
}))(MuiDialogContent);

function AccountPublications(props) {
    const classes = AccountPublicationStyle();
    const {
        postImage,
        postComments,
        postLikes,
        postCommentItems,
        userName,
        toggleLikes,
        favoritesPost,
        postDate,
        // makeComment,
        profilePhoto,
        postId,
    } = props;
    const [open, setOpen] = useState(false);
    const [saved, setSaved] = useState(false);
    // const [comment, setComment] = useState(false);
    // const [emojiPickerState, SetEmojiPicker] = useState(false);
    // const [message, SetMessage] = useState('');
    const [favorites, setFavorites] = useState(favoritesPost);

    const handleFavoritesClick = (postId) => {
        setFavorites(!favorites);
        toggleLikes(postId);
    };
    const handleSaveClick = () => {
        setSaved(!saved);
    };
    // const handleCommentClick = () => {
    //     setComment(!comment);
    // };

    // function triggerPicker(event) {
    //     event.preventDefault();
    //     SetEmojiPicker(!emojiPickerState);
    // }

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    let commentText = '';
    let commentPostedBy = '';

    const renderComments = postCommentItems.map((commentItem, index) => {
        commentText = commentItem.text;
        commentPostedBy = commentItem.postedBy.name;
        return (
            <Box key={index}>
                {commentPostedBy}
                {commentText}
            </Box>
        );
    });

    return (
        <Box className={classes.instaPhotoWrapperColumn}>
            {open ? (
                <Dialog
                    maxWidth="lg"
                    onClose={handleClose}
                    aria-labelledby="customized-dialog-title"
                    open={open}
                >
                    <Box component="div" className={classes.dialogWrapper}>
                        <Box
                            component="div"
                            className={classes.dialogPhotoWrapper}
                        >
                            <Box
                                component="img"
                                className={classes.dialogPhotoItem}
                                src={postImage}
                            />
                        </Box>
                        <Box component="div">
                            <DialogTitle
                                id="customized-dialog-title"
                                onClose={handleClose}
                            >
                                <AvatarAccountDialog
                                    userName={userName}
                                    profilePhoto={profilePhoto}
                                />
                            </DialogTitle>
                            <DialogContent dividers>
                                <Box
                                    component="div"
                                    className={classes.dialogContentComments}
                                >
                                    {renderComments}
                                </Box>
                            </DialogContent>
                            <DialogContent>
                                <CardActions
                                    disableSpacing
                                    className={classes.cardActionWrapper}
                                >
                                    <Box component="div">
                                        <IconButton
                                            onClick={() =>
                                                handleFavoritesClick(postId)
                                            }
                                            aria-expanded={favorites}
                                            aria-label="add to favorites"
                                        >
                                            <Box
                                                component="div"
                                                className={
                                                    classes.instaFavoriteIconWrapper
                                                }
                                            >
                                                <Box
                                                    component="span"
                                                    className={clsx(
                                                        classes.instaFavoriteIconFalse,
                                                        {
                                                            [classes.instaFavoriteIconTrue]: favorites,
                                                        }
                                                    )}
                                                />
                                            </Box>
                                        </IconButton>
                                        <IconButton
                                        // aria-label="comment"
                                        // aria-expanded={comment}
                                        // onClick={handleCommentClick}
                                        >
                                            <Box
                                                component="span"
                                                className={
                                                    classes.instaCommentIcon
                                                }
                                            />
                                        </IconButton>
                                        <IconButton
                                            aria-label="send-message"
                                            // onClick={triggerPicker}
                                        >
                                            <Box
                                                component="span"
                                                className={
                                                    classes.instaSendMessage
                                                }
                                            />
                                        </IconButton>
                                    </Box>
                                    <Box component="div">
                                        <IconButton
                                            aria-label="save"
                                            onClick={handleSaveClick}
                                            aria-expanded={saved}
                                        >
                                            <Box
                                                component="div"
                                                className={
                                                    classes.instaSaveIconWrapper
                                                }
                                            >
                                                <Box
                                                    component="span"
                                                    className={clsx(
                                                        classes.instaSaveIconFalse,
                                                        {
                                                            [classes.instaSaveIconTrue]: saved,
                                                        }
                                                    )}
                                                />
                                            </Box>
                                        </IconButton>
                                    </Box>
                                </CardActions>
                                <CardContent>
                                    <Typography
                                        className={classes.postLikes}
                                        variant="body2"
                                        color="textPrimary"
                                        component="span"
                                    >
                                        {postLikes} Likes
                                    </Typography>
                                    <Typography
                                        className={classes.postDate}
                                        variant="body2"
                                        color="textPrimary"
                                        component="span"
                                    >
                                        {postDate}
                                    </Typography>
                                </CardContent>
                                {/* <form
                                    noValidate
                                    onSubmit={(e) => {
                                        e.preventDefault();
                                        makeComment(e.target[0].value, postId);
                                    }}
                                    className={classes.instaCommentTrue}
                                >
                                    <TextField
                                        // id="standard-textarea"
                                        label="Добавить комментарий..."
                                        placeholder="Ваш комментарий..."
                                        multiline
                                        fullWidth
                                        onChange={(event) =>
                                            SetMessage(event.target.value)
                                        }
                                        value={message}
                                    />
                                    <Button
                                        color="primary"
                                        onClick={handleCommentClick}
                                        className={classes.publishButton}
                                        type="submit"
                                    >
                                        Опубликовать
                                    </Button>
                                </form> */}
                                {/* <div className="emoji-table">
                                    {emojiPickerState && (
                                        <Picker
                                            title="Pick your emoji…"
                                            emoji="point_up"
                                            onSelect={(emoji) =>
                                                SetMessage(
                                                    message + emoji.native,
                                                )
                                            }
                                        />
                                    )}
                                </div> */}
                            </DialogContent>
                        </Box>
                    </Box>
                </Dialog>
            ) : null}
            <Box
                component="div"
                className={classes.instaPhotoItem}
                onClick={handleClickOpen}
            >
                <Box
                    component="div"
                    className={classes.instaPhotoItemDisplayNoneBox}
                >
                    <Box
                        component="div"
                        className={classes.instaPhotoItemFavorites}
                    >
                        <Box
                            component="span"
                            className={classes.instaPhotoItemFavoritesIcon}
                        />
                        <Box
                            component="span"
                            className={classes.instaPhotoItemFavoritesCount}
                        >
                            {postLikes}
                        </Box>
                    </Box>
                    <Box
                        component="div"
                        className={classes.instaPhotoItemComments}
                    >
                        <Box
                            component="span"
                            className={classes.instaPhotoItemCommentsIcon}
                        />
                        <Box
                            component="span"
                            className={classes.instaPhotoItemCommentsCount}
                        >
                            {postComments}
                        </Box>
                    </Box>
                </Box>
                <CardMedia
                    component="img"
                    className={classes.instaPhotoItemImg}
                    src={postImage}
                />
            </Box>
        </Box>
    );
}

export default AccountPublications;

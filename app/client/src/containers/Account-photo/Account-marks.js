import Box from '@material-ui/core/Box';
import CardMedia from '@material-ui/core/CardMedia';
import React from 'react';
import instPhoto1 from '../../assets/img/Photo-for-test/brad-pitt.jpg';
import instSpriteToggle from '../../assets/img/sprites/576406ccc24b.png';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    instaPhotoWrapperColumn: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        maxWidth: '975px',
        margin: '0 auto',
        width: '100%',
    },
    instaPhotoItem: {
        width: '305px',
        height: '305px',
        marginRight: '30px',
        position: 'relative',
        '&:last-child': {
            marginRight: '0',
        },
    },
    instaPhotoItemImg: {
        width: '305px',
        height: '305px',
        background: `url( ${instPhoto1})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    instaPhotoItemDisplayNoneBox: {
        position: 'absolute',
        width: '305px',
        height: '305px',
        backgroundColor: '#262626',
        opacity: '0',
        zIndex: '1250',
        '&:hover': {
            opacity: '0.5',
        },
    },
    instaPhotoItemFavorites: {},
    instaPhotoItemFavoritesIcon: {
        position: 'absolute',
        top: '148px',
        left: '100px',
        width: '19px',
        height: '17px',
        background: `url( ${instSpriteToggle})`,
        backgroundPosition: '-384px -126px',
        backgroundSize: 'initial',
    },
    instaPhotoItemFavoritesCount: {
        position: 'absolute',
        top: '145px',
        left: '125px',
        color: '#fafafa',
        fontWeight: '600',
    },
    instaPhotoItemComments: {},
    instaPhotoItemCommentsIcon: {
        position: 'absolute',
        top: '148px',
        left: '165px',
        width: '19px',
        height: '17px',
        background: `url( ${instSpriteToggle})`,
        backgroundPosition: '-384px -167px',
        backgroundSize: 'initial',
    },
    instaPhotoItemCommentsCount: {
        position: 'absolute',
        top: '145px',
        left: '190px',
        color: '#fafafa',
        fontWeight: '600',
    },
}));

function AccountMarks() {
    const classes = useStyles();

    return (
        <Box component="div" className={classes.instaPhotoWrapperColumn}>
            <Box on component="div" className={classes.instaPhotoItem}>
                <Box
                    component="div"
                    className={classes.instaPhotoItemDisplayNoneBox}
                >
                    <Box
                        component="div"
                        className={classes.instaPhotoItemFavorites}
                    >
                        <Box
                            component="span"
                            className={classes.instaPhotoItemFavoritesIcon}
                        />
                        <Box
                            component="span"
                            className={classes.instaPhotoItemFavoritesCount}
                        >
                            28
                        </Box>
                    </Box>
                    <Box
                        component="div"
                        className={classes.instaPhotoItemComments}
                    >
                        <Box
                            component="span"
                            className={classes.instaPhotoItemCommentsIcon}
                        />
                        <Box
                            component="span"
                            className={classes.instaPhotoItemCommentsCount}
                        >
                            9
                        </Box>
                    </Box>
                </Box>
                <CardMedia
                    component="img"
                    className={classes.instaPhotoItemImg}
                />
            </Box>
            <Box component="div" className={classes.instaPhotoItem}>
                <Box
                    component="div"
                    className={classes.instaPhotoItemDisplayNoneBox}
                >
                    <Box
                        component="div"
                        className={classes.instaPhotoItemFavorites}
                    >
                        <Box
                            component="span"
                            className={classes.instaPhotoItemFavoritesIcon}
                        />
                        <Box
                            component="span"
                            className={classes.instaPhotoItemFavoritesCount}
                        >
                            24
                        </Box>
                    </Box>
                    <Box
                        component="div"
                        className={classes.instaPhotoItemComments}
                    >
                        <Box
                            component="span"
                            className={classes.instaPhotoItemCommentsIcon}
                        />
                        <Box
                            component="span"
                            className={classes.instaPhotoItemCommentsCount}
                        >
                            4
                        </Box>
                    </Box>
                </Box>
                <CardMedia
                    component="img"
                    className={classes.instaPhotoItemImg}
                />
            </Box>
            <Box component="div" className={classes.instaPhotoItem}>
                <Box
                    component="div"
                    className={classes.instaPhotoItemDisplayNoneBox}
                >
                    <Box
                        component="div"
                        className={classes.instaPhotoItemFavorites}
                    >
                        <Box
                            component="span"
                            className={classes.instaPhotoItemFavoritesIcon}
                        />
                        <Box
                            component="span"
                            className={classes.instaPhotoItemFavoritesCount}
                        >
                            50
                        </Box>
                    </Box>
                    <Box
                        component="div"
                        className={classes.instaPhotoItemComments}
                    >
                        <Box
                            component="span"
                            className={classes.instaPhotoItemCommentsIcon}
                        />
                        <Box
                            component="span"
                            className={classes.instaPhotoItemCommentsCount}
                        >
                            11
                        </Box>
                    </Box>
                </Box>
                <CardMedia
                    component="img"
                    className={classes.instaPhotoItemImg}
                />
            </Box>
        </Box>
    );
}

export default AccountMarks;

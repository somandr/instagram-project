import React from 'react';
import AvatarSideRecomendedFriendList from '../../components/Avatar/Avatar-side-recomended-friend-list';
import Box from '@material-ui/core/Box';
import RecomendedFriendListStyles from '../../assets/styles/RecomendedFriendListStyles';

export default function RecomendedFriendList(props) {
    const classes = RecomendedFriendListStyles();
    const { users, userId } = props;

    const recomendedFriendsAll = users.map((post) => {
        const uniqueIdArr = post.followers;
        const allItem = uniqueIdArr.includes(userId) ? undefined : post;
        return allItem;
    });

    const recomendedFriendsArr = recomendedFriendsAll.filter((item) => {
        return item !== undefined;
    });

    const recomendedFriendsFirstFive = recomendedFriendsArr.slice(0, 5);

    const renderRecomendedFriend = recomendedFriendsFirstFive.map(
        (post, index) => {
            if (post._id === userId) {
                // eslint-disable-next-line array-callback-return
                return;
            }
            const pathToProfileStories =
                post._id !== userId ? `/profile/${post._id}` : '/profile/user';

            const photoJpeg = post.photos[0].includes('jpeg')
                ? post.photos[0]
                : post.photos[1];

            return (
                <AvatarSideRecomendedFriendList
                    key={index}
                    title={post.username}
                    postedByProfilePhoto={!post.photos ? undefined : photoJpeg}
                    userId={post._id}
                    pathToProfileStories={pathToProfileStories}
                />
            );
        }
    );
    return (
        <div className={classes.root}>
            <Box component="div">
                <Box component="p" className={classes.subHeader}>
                    Рекомендации для вас
                </Box>
            </Box>
            <Box component="div">
                <Box component="div">{renderRecomendedFriend}</Box>
            </Box>
            <Box component="div" className={classes.footer}>
                <Box component="ul" className={classes.footerUl}>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Информация
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Помощь
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Пресса
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            API
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Вакансии
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Кофиденциальность
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Условия
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Места
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Популярные аккаунты
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Хештеги
                        </Box>
                    </Box>
                    <Box component="li" className={classes.footerLi}>
                        <Box
                            component="a"
                            href="#"
                            className={classes.footerLinks}
                        >
                            Язык
                        </Box>
                    </Box>
                </Box>
                <Box component="span" className={classes.footerCopyright}>
                    &copy; Instagram от Facebook,{' '}
                    {new Date().toLocaleString('en', { year: 'numeric' })}
                </Box>
            </Box>
        </div>
    );
}

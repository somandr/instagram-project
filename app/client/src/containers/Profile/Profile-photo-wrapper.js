import Box from '@material-ui/core/Box';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import AccountPublications from '../Account-photo/Account-publications';
import instSpriteToggle from '../../assets/img/sprites/576406ccc24b.png';

const useStyles = makeStyles(() => ({
    toggleButtonsWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: '975px',
        margin: '0 auto',
        marginBottom: '20px',
    },
    toggleButtonsPublications: {
        paddingTop: '20px',
        borderTop: '1px solid #262626',
        textDecoration: 'none',
        color: '#262626',
        marginRight: '80px',
        cursor: 'pointer',
    },
    toggleButtonsPublicationsOpacity: {
        paddingTop: '20px',
        textDecoration: 'none',
        color: '#262626',
        cursor: 'pointer',
        opacity: '0.5',
    },
    toggleButtonsPublicationsWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    toggleButtonsPublicationsIcon: {
        width: '12px',
        height: '12px',
        background: `url( ${instSpriteToggle})`,
        backgroundPosition: '-263px -288px',
        backgroundSize: 'initial',
    },
    toggleButtonsPublicationsText: {
        textTransform: 'uppercase',
        fontSize: '12px',
        fontWeight: '600',
        marginLeft: '6px',
    },
    toggleButtonsMarks: {
        paddingTop: '20px',
        borderTop: '1px solid #262626',
        textDecoration: 'none',
        color: '#262626',
        cursor: 'pointer',
    },
    toggleButtonsMarksOpacity: {
        paddingTop: '20px',
        textDecoration: 'none',
        color: '#262626',
        cursor: 'pointer',
        opacity: '0.5',
    },
    toggleButtonsMarksWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    toggleButtonsMarksIcon: {
        width: '12px',
        height: '12px',
        background: `url( ${instSpriteToggle})`,
        backgroundPosition: '-319px -288px',
        backgroundSize: 'initial',
    },
    toggleButtonsMarksText: {
        textTransform: 'uppercase',
        fontSize: '12px',
        fontWeight: '600',
        marginLeft: '6px',
    },
    instaPhotoWrapper: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        maxWidth: '975px',
        margin: '0 auto',
        width: '100%',
    },
}));

export default function ProfilePhotoWrapper() {
    const [userPosts] = useState([]);
    const classes = useStyles();

    const renderPosts = userPosts.map((post, index) => {
        return <AccountPublications postImage={post.photos[1]} key={index} />;
    });

    return (
        <div>
            <Box component="div" className={classes.toggleButtonsWrapper}>
                <Box component="div" className={classes.toggleButtonsWrapper}>
                    <Box
                        component="div"
                        className={classes.toggleButtonsPublications}
                    >
                        <Box
                            component="span"
                            className={classes.toggleButtonsPublicationsWrapper}
                        >
                            <Box
                                component="span"
                                className={
                                    classes.toggleButtonsPublicationsIcon
                                }
                            />
                            <Box
                                component="span"
                                className={
                                    classes.toggleButtonsPublicationsText
                                }
                            >
                                публикации
                            </Box>
                        </Box>
                    </Box>

                    <Box component="div" className={classes.toggleButtonsMarks}>
                        <Box
                            component="span"
                            className={classes.toggleButtonsMarksWrapper}
                        >
                            <Box
                                component="span"
                                className={classes.toggleButtonsMarksIcon}
                            />
                            <Box
                                component="span"
                                className={classes.toggleButtonsMarksText}
                            >
                                отметки
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Box>
            <Box component="div" className={classes.instaPhotoWrapper}>
                <Box component="div">{renderPosts}</Box>
                <Box component="div">
                    <Box component="p">Привет</Box>
                </Box>
            </Box>
        </div>
    );
}

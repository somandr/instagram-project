import { makeStyles } from '@material-ui/core/styles';
import instBigIconSprite from '../img/sprites/576406ccc24b.png';
import instSprite from '../img/sprites/c14ffe44a4f6.png';

const PostCardStyles = makeStyles({
    root: {
        marginBottom: 30,
    },
    postCardsWrap: {
        maxWidth: 300,
    },

    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    cardMediaWrapper: {
        height: '345px',
        width: '614px',
        position: 'relative',
    },
    cardMediaFavoriteIconFalse: {
        opacity: '0',
    },
    cardMediaFavoriteIconTrue: {
        display: 'flex',
        position: 'absolute',
        top: '42%',
        left: '44%',
        width: '88px',
        height: '76px',
        background: `url( ${instBigIconSprite})`,
        backgroundPosition: '-2px -100px',
        backgroundSize: 'initial',
        opacity: '0.4',
    },

    cardMediaContent: {
        width: '100%',
    },
    cardActionWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    instaFavoriteIconWrapper: {
        display: 'flex',
        height: '30px',
        width: '30px',
        margin: '0 auto',
        cursor: 'pointer',
        justifyContent: 'center',
        alignItems: 'center',
    },
    instaFavoriteIconFalse: {
        display: 'flex',
        width: '26px',
        height: '23px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-232px -370px',
        backgroundSize: 'initial',
    },
    instaFavoriteIconTrue: {
        display: 'flex',
        width: '26px',
        height: '23px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-206px -370px',
        backgroundSize: 'initial',
    },
    instaCommentIcon: {
        display: 'flex',
        width: '26px',
        height: '24px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-337px -344px',
        backgroundSize: 'initial',
    },
    instaSendMessage: {
        display: 'flex',
        width: '26px',
        height: '24px',
        background: `url( ${instSprite})`,
        backgroundPosition: '0 -368px',
        backgroundSize: 'initial',
    },

    instaSaveIconWrapper: {
        display: 'flex',
        height: '30px',
        width: '30px',
        margin: '0 auto',
        cursor: 'pointer',
        justifyContent: 'center',
        alignItems: 'center',
    },
    instaSaveIconFalse: {
        display: 'flex',
        width: '24px',
        height: '24px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-389px -396px',
        backgroundSize: 'initial',
    },
    instaSaveIconTrue: {
        display: 'flex',
        width: '24px',
        height: '24px',
        background: `url( ${instSprite})`,
        backgroundPosition: '-311px -396px',
        backgroundSize: 'initial',
    },
    instaCommentFalse: {
        display: 'none',
    },
    instaCommentTrue: {
        display: 'flex',
        width: '100%',
        paddingLeft: 20,
    },
    publishButton: {
        margin: '0 40px',
        textDecoration: 'none',
    },
    avatar: {
        backgroundColor: '#ffffff',
    },
    avatarTitle: {
        textDecoration: 'none',
        color: '#262626',
        textTransform: 'lowercase',
        fontSize: '14px',
    },
    commentsWrap: {
        width: '100%',
    },
    btnMoreComments: {
        color: 'grey',
        fontSize: 12,
    },
    deleteIcon: {
        fontSize: 28,
        color: 'grey',
        cursor: 'pointer',
    },
    '@media screen and (max-width: 700px)': {
        root: {
            width: '400px',
            // paddingLeft: 10,
        },
    },
    '@media screen and (max-width: 400px)': {
        root: {
            width: '380px',
            // paddingLeft: 10,
        },
    },
});
export default PostCardStyles;

import { makeStyles } from '@material-ui/core/styles';
import instSprite from '../img/sprites/576406ccc24b.png';

const MainStyles = makeStyles((theme) => ({
    main: {
        paddingTop: '80px',
        minHeight: '100vh',
        justifyContent: 'center',
    },
    mainContainer: {
        paddingTop: '5px',
        display: 'flex',
        maxWidth: '975px',
        margin: '0 auto',
        position: 'relative',
        width: '100%',
    },
    usersStories: {
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        height: '116px',
        maxWidth: '614px',
        width: '614px',
        marginBottom: 24,
        border: '1px solid #EDEDED',
        borderRadius: 3,
        position: 'relative',
    },
    imgLogoStories: {
        position: 'absolute',
        opacity: 0.8,
        background: `url( ${instSprite})`,
        width: '67px',
        height: '67px',
        backgroundPosition: 'bottom 132px left 158px',
        backgroundSize: 'initial',
    },
    textLogoStories: {
        display: 'flex',
        fontSize: 12,
        textAlign: 'center',
        justifyContent: 'center',
        textDecoration: 'none',
        paddingTop: 5,
    },
    wrapStories: {
        marginLeft: 20,
        paddingBottom: 7,
        height: '100%',

        '& a': {
            color: '#262626',
            fontSize: 12,
        },
    },
    avatar: {
        position: 'relative',
        width: 56,
        height: 56,
        margin: '5px 7px 5px 7px',
    },
    postCard: {
        maxWidth: '614px',
    },

    sideListWrapper: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'inline-block',
            position: 'fixed',
            left: '615px',
            maxWidth: '293px',
        },
        [theme.breakpoints.up('md')]: {
            display: 'inline-block',
            position: 'fixed',
            left: '700px',
            maxWidth: '293px',
        },
        [theme.breakpoints.up('lg')]: {
            display: 'inline-block',
            position: 'fixed',
            left: '840px',
            maxWidth: '293px',
        },
        [theme.breakpoints.up('xl')]: {
            display: 'inline-block',
            position: 'fixed',
            left: '1040px',
            maxWidth: '293px',
        },
    },
    cardMediaWrapper: {
        border: 'none',
    },
    cardMediaContent: {
        border: 'none',
    },
    postLoader: {
        color: 'grey',
    },
    postLoaderWrap: {
        width: '100%',
        textAlign: 'center',
    },
    '@media screen and (max-width: 910px)': {
        sideListWrapper: {
            display: 'none',
        },
        mainContainer: {
            width: '100%',
            margin: '0 auto',
            display: 'flex',
            position: 'relative',
            maxWidth: 975,
            paddingTop: 5,
            justifyContent: 'center',
        },
    },
    '@media screen and (max-width: 700px)': {
        usersStories: {
            backgroundColor: '#FFFFFF',
            alignItems: 'center',
            height: '116px',
            // maxWidth: '614px',
            width: '100%',
            marginBottom: 24,
            border: '1px solid #EDEDED',
            borderRadius: 3,
            position: 'relative',
        },
        main: {
            display: 'flex',
            paddingTop: '80px',
            minHeight: '100vh',
            justifyContent: 'center',
        },
        mainContainer: {
            width: '100%',
            margin: '0 auto',
            display: 'flex',
            position: 'relative',
            maxWidth: 975,
            paddingTop: 5,
            justifyContent: 'center',
        },
    },
    ringStories: {
        background: `url( ${instSprite})`,
        width: '180px',
        height: '52px',
        backgroundPosition: '-97px 0px',
        backgroundSize: 'initial',
    },
}));
export default MainStyles;

import { makeStyles } from '@material-ui/core/styles';
import instSprite from '../img/sprites/576406ccc24b.png';

const AppBarStyles = makeStyles({
    header: {
        height: '54px',
        width: '100%',
        borderBottom: '1px solid grey',
        position: 'fixed',
        backgroundColor: '#fafafa',
        zIndex: '1300',
    },
    instaLogoFormWrapper: {
        display: 'flex',
        width: '975px',
        height: '54px',
        margin: '0 auto',
        padding: '0 20px',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    instaLogoForm: {
        display: 'flex',
        width: '103px',
        height: '29px',
        background: `url( ${instSprite})`,
        backgroundPosition: '0 -288px',
        backgroundSize: 'initial',
    },
});

export default AppBarStyles;

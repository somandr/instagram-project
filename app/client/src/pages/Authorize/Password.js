import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { Grid, Box } from '@material-ui/core';
import { useHttp } from '../../hooks/http.hook';

import AuthStyles from '../../assets/styles/AuthStyles';
import Input from '../../components/Input';
import ButtonMain from '../../components/ButtonMain';

const Password = () => {
    const { request } = useHttp();
    const [btn, setBtn] = useState(true);
    const [form, setForm] = useState({
        email: '',
    });
    const classes = AuthStyles();

    const changeHandler = (event) => {
        if (event.target.value.length >= 1) {
            setBtn(false);
        } else {
            setBtn(true);
        }
        setForm({ ...form, [event.target.type]: event.target.value });
    };

    const passwordHandler = async (formData) => {
        try {
            await request('/api/authorize/password-recovery', 'POST', {
                ...formData,
            });
            alert(
                'Ссылка, для восстановления пароля, отправлена вам на эл. почту!'
            );
        } catch (e) {}
    };

    return (
        <Box component="section" className={classes.signInSection}>
            <Grid
                container
                justify="center"
                alignItems="center"
                component="main"
                className={classes.signInContainer}
            >
                <Grid item className={classes.signInFormWrapper}>
                    <Box
                        component="form"
                        className={classes.passwordForm}
                        method="post"
                    >
                        <Box
                            component="div"
                            className={classes.instaLogoFormWrapper}
                        >
                            <Box className={classes.passwordWrapper} />
                        </Box>
                        <Box className={classes.inputsWrapper}>
                            <Box className={classes.textWrapper}>
                                <Box
                                    component="span"
                                    className={classes.textTitle}
                                >
                                    Не удаётся войти?
                                </Box>
                                <Box
                                    component="p"
                                    className={classes.textPassword}
                                >
                                    Введите имя пользователя или электронный
                                    адрес, и мы отправим вам ссылку для
                                    восстановления доступа к аккаунту.
                                </Box>
                            </Box>
                            <Input
                                fullWidth
                                autoComplete="email"
                                type="email"
                                placeholder="Эл. адрес, телефон или имя пользователя"
                                variant="filled"
                                onChange={changeHandler}
                                disableUnderline={true}
                                autoFocus={false}
                            />
                            <ButtonMain
                                onClick={() => passwordHandler(form)}
                                disabled={btn}
                            >
                                Получите ссылку для входа
                            </ButtonMain>
                            <Box component="span" className={classes.devider}>
                                или
                            </Box>
                            <Box className={classes.linkBottomPassword}>
                                <NavLink to="/signup">
                                    Создать новый аккаунт
                                </NavLink>
                            </Box>
                        </Box>
                        <Box className={classes.footerPassword}>
                            <NavLink to="/login" className={classes.linkFooter}>
                                Вернуться к входу
                            </NavLink>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    );
};

export default Password;

import React, { useState } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { Grid, Box } from '@material-ui/core';
import { useHttp } from '../../hooks/http.hook';

import AuthStyles from '../../assets/styles/AuthStyles';
import Input from '../../components/Input';
import ButtonMain from '../../components/ButtonMain';

const PasswdRecover = () => {
    const { request } = useHttp();
    const [btn, setBtn] = useState(true);
    const [form, setForm] = useState({
        password: '',
    });
    const { token } = useParams();
    console.log(token);
    const classes = AuthStyles();

    const changeHandler = (event) => {
        if (event.target.value.length >= 6) {
            setBtn(false);
        } else {
            setBtn(true);
        }
        setForm({ ...form, [event.target.type]: event.target.value });
    };

    const passwordHandler = async (formData) => {
        try {
            const data = await request('/api/authorize/password-new', 'POST', {
                ...formData,
                token,
            });
            console.log(data);
        } catch (e) {}
    };

    return (
        <Box component="section" className={classes.signInSection}>
            <Grid
                container
                justify="center"
                alignItems="center"
                component="main"
                className={classes.signInContainer}
            >
                <Grid item className={classes.signInFormWrapper}>
                    <Box
                        component="form"
                        className={classes.passwordForm}
                        method="post"
                    >
                        <Box
                            component="div"
                            className={classes.instaLogoFormWrapper}
                        >
                            <Box className={classes.passwordWrapper} />
                        </Box>
                        <Box className={classes.inputsWrapper}>
                            <Box className={classes.textWrapper}>
                                <Box
                                    component="span"
                                    className={classes.textTitle}
                                >
                                    Восстановление пароля?
                                </Box>
                                <Box
                                    component="p"
                                    className={classes.textPassword}
                                >
                                    Введите новый пароль, для восстановления
                                    доступа к аккаунту.
                                </Box>
                            </Box>
                            <Input
                                fullWidth
                                autoComplete="password"
                                type="password"
                                placeholder="Введите новый пароль"
                                variant="filled"
                                onChange={changeHandler}
                                disableUnderline={true}
                                autoFocus={false}
                            />
                            <ButtonMain
                                onClick={() => passwordHandler(form)}
                                disabled={btn}
                            >
                                Изменить пароль
                            </ButtonMain>
                            <Box component="span" className={classes.devider}>
                                или
                            </Box>
                            <Box className={classes.linkBottomPassword}>
                                <NavLink to="/signup">
                                    Создать новый аккаунт
                                </NavLink>
                            </Box>
                        </Box>
                        <Box className={classes.footerPassword}>
                            <NavLink to="/login" className={classes.linkFooter}>
                                Вернуться к входу
                            </NavLink>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    );
};

export default PasswdRecover;

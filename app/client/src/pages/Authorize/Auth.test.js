import React from 'react';
import Auth from './Auth';
import { act } from 'react-dom/test-utils';
import { render, unmountComponentAtNode } from 'react-dom';

let container = null;
beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
});

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

describe('Testing Auth', () => {
    test('Auth are rendered successfully', () => {
        act(() => {
            render(<Auth />, container);
        });
    });
});

import React, { useState, useEffect } from 'react';
import { useHttp } from '../../hooks/http.hook';
import { SignUp } from '../../components/Auth/SignUp';
import { Loader } from '../../components/Loader/Loader';

import Message from '../../components/Message/Message';
import { useHistory } from 'react-router-dom';

const Registration = () => {
    const { loading, request, error, clearError } = useHttp();
    const [openPopover, setOpenPopover] = useState(false);
    const [textError, setTextError] = useState('');
    const history = useHistory();

    useEffect(() => {
        if (error) {
            setTextError(error);
            setOpenPopover(true);
            setTimeout(() => {
                setOpenPopover(false);
            }, 6000);
        }
        clearError();
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    }, [error, clearError]);

    if (loading) {
        return <Loader />;
    }

    const closePopoverHandler = () => {
        setOpenPopover(false);
    };

    const signUpHandler = async (formData) => {
        formData.username = formData.username.toLowerCase();
        try {
            const response = await request('/api/authorize/signup', 'POST', {
                ...formData,
            });
            if (response) {
                alert(response.message);
                history.push('/');
            }
        } catch (e) {}
    };
    return (
        <>
            <SignUp signUpHandler={signUpHandler} />
            <Message
                openPopover={openPopover}
                textError={textError}
                closePopoverHandler={closePopoverHandler}
            />
        </>
    );
};

export default Registration;

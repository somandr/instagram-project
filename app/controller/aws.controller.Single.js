const s3 = require('../s3/config');
const sharp = require('sharp');
const crypto = require('crypto');
const User = require('../models/User');

let arr = [];

exports.doUpload = (req, res) => {
    // eslint-disable-next-line prefer-destructuring
    const s3Client = s3.s3Client;
    const params = s3.uploadParams;

    const paramResize = [
        { width: 300, height: 300, format: 'webp' },
        { width: 300, height: 300, format: 'jpeg' },
    ];

    const image = sharp(req.file.buffer);
    paramResize.map((param) => {
        let hex;
        crypto.randomBytes(5, (err, buf) => {
            if (err) {
                // eslint-disable-next-line no-console
                return console.log(err);
            }
            hex = `${buf.toString('hex')}_${req.file.fieldname}`;
        });
        return image
            .toFormat(param.format)
            .resize(param.width, param.height, {
                fit: 'outside',
                position: sharp.strategy.attention,
                background: { r: 255, g: 255, b: 255, alpha: 0 },
            })
            .withMetadata()
            .toBuffer({ resolveWithObject: true })
            .then((data) => {
                const file = data.data;
                const { width, height, format } = data.info;

                params.Key = `${hex}_${width}x${height}.${format}`;
                params.Body = file;

                s3Client.upload(params, (err, dataNew) => {
                    arr.push(dataNew.Location);
                });
            });
    });

    setTimeout(() => {
        User.findByIdAndUpdate(
            req.body.userId,
            { $set: { photos: arr } },
            { new: true },
            (err, result) => {
                res.json(result);
            }
        );
    }, 4000);
    arr = [];
};

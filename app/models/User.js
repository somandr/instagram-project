const { Schema, model, Types } = require('mongoose');

const schema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    resetToken: String,
    expireToken: Date,
    photos: {
        type: [String],
        default:
            'https://www.meme-arsenal.com/memes/5eae5104f379baa355e031fa1ded886c.jpg',
    },
    followers: [{ type: Types.ObjectId, ref: 'User' }],
    following: [{ type: Types.ObjectId, ref: 'User' }],
});

module.exports = model('User', schema);

const { Schema, model, Types } = require('mongoose');

const schema = new Schema(
    {
        photos: [String],

        body: {
            type: String,
        },
        likes: [{ type: Types.ObjectId, ref: 'User' }],

        comments: [
            {
                text: String,
                postedBy: { type: Types.ObjectId, ref: 'User' },
            },
        ],
        filename: {
            type: String,
        },
        postedBy: {
            type: Types.ObjectId,
            ref: 'User',
        },
    },
    { timestamps: true }
);

module.exports = model('Post', schema);

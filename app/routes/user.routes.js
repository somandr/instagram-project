const { Router } = require('express');
const User = require('../models/User');
const Post = require('../models/Post');
const awsController = require('./../controller/aws.controller.Single');
const multerUploads = require('./../multer/config');
const auth = require('../middleware/authorization.middleware');

const router = Router();

// =======================> /api/authorized/user/profile/user/:id
router.get('/profile/user/:id', auth, (req, res) => {
    User.findOne({ _id: req.userId })
        .select('-password')
        .then((userInfo) => {
            User.findOne({ _id: req.params.id })
                .select('-password')
                .populate('followers', '-password')
                .populate('following', '-password')
                .then((userFollowers) => {
                    User.findOne({ _id: req.params.id })
                        .select('-password')
                        .then((user) => {
                            Post.find({
                                postedBy: req.params.id,
                            })
                                .populate('postedBy', '-password')
                                .exec((err, posts) => {
                                    if (err) {
                                        return res
                                            .status(422)
                                            .json({ error: err });
                                    }
                                    res.json({
                                        userInfo,
                                        user,
                                        posts,
                                        userFollowers,
                                    });
                                });
                        });
                });
        })
        .catch((error) => {
            // eslint-disable-next-line no-console
            console.log(error);
            return res.status(404).json({ error: 'User not found :(' });
        });
});

// =======================> /api/authorized/user/profile/user
router.get('/profile/user', auth, (req, res) => {
    User.findOne({ _id: req.userId })
        .select('-password')
        .populate('followers', '-password')
        .populate('following', '-password')
        .then((userFollowers) => {
            User.findOne({ _id: req.userId })
                .select('-password')
                .then((user) => {
                    Post.find({
                        postedBy: req.userId,
                    })
                        .populate('postedBy', '-password')
                        .exec((err, posts) => {
                            if (err) {
                                return res.status(422).json({ error: err });
                            }
                            const userInfo = user;
                            res.json({ userInfo, user, posts, userFollowers });
                        });
                });
        })
        .catch((error) => {
            // eslint-disable-next-line no-console
            console.log(error);
            return res.status(404).json({ error: 'User not found :(' });
        });
});

// =======================> /api/authorized/user/profile/photo
router.put(
    '/profile/photo',
    auth,
    multerUploads.single('img'),
    awsController.doUpload
);

// =======================> /api/authorized/user/follow
router.post('/follow', auth, (req, res) => {
    User.findByIdAndUpdate(
        req.body.followId,
        {
            $push: { followers: req.userId },
        },
        {
            new: true,
        },
        (err, userFollow) => {
            if (err) {
                return res.status(422).json({ error: err });
            }
            User.findByIdAndUpdate(
                req.userId,
                {
                    $push: { following: req.body.followId },
                },
                { new: true }
            )
                .select('-password')
                .then((user) => {
                    res.json({ user, userFollow });
                })
                .catch((error) => {
                    // eslint-disable-next-line no-console
                    console.log(error);
                    return res.status(422).json({ error: err });
                });
        }
    );
});

// =======================> /api/authorized/user/unfollow
router.post('/unfollow', auth, (req, res) => {
    User.findByIdAndUpdate(
        req.body.unfollowId,
        {
            $pull: { followers: req.userId },
        },
        {
            new: true,
        },
        (err, userFollow) => {
            if (err) {
                return res.status(422).json({ error: err });
            }
            User.findByIdAndUpdate(
                req.userId,
                {
                    $pull: { following: req.body.unfollowId },
                },
                { new: true }
            )
                .select('-password')
                .then((user) => {
                    res.json({ user, userFollow });
                })
                .catch((error) => {
                    // eslint-disable-next-line no-console
                    console.log(error);
                    return res.status(422).json({ error: err });
                });
        }
    );
});

// =======================> /api/authorized/user/search
router.post('/search', auth, (req, res) => {
    let userPattern = new RegExp(req.body.query);
    User.find({ username: { $regex: userPattern } })
        .select('_id username name photos')
        .then((user) => {
            res.json({ user });
        })
        .catch((error) => {
            // eslint-disable-next-line no-console
            console.log(error);
        });
});

module.exports = router;

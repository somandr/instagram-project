const { Router } = require('express');
const awsController = require('../controller/aws.controller');
const multerUploads = require('./../multer/config');
const Post = require('../models/Post');
const User = require('../models/User');
const auth = require('../middleware/authorization.middleware');

const router = Router();

// =======================> /api/authorized/post/getsubpost
router.get('/getsubpost', auth, (req, res) => {
    User.find()
        .select('-password')
        .then((users) => {
            User.findOne({ _id: req.userId })
                .select('-password')
                .then((user) => {
                    const posts = [...user.following, req.userId];
                    Post.find({ postedBy: { $in: posts } })
                        .populate('postedBy', '-password')
                        .populate('comments.postedBy', '_id name username')
                        .sort('-createdAt')
                        .then((mypost) => {
                            res.json({ user, users, mypost });
                        })
                        .catch((err) => {
                            // eslint-disable-next-line no-console
                            console.log(err);
                        });
                })
                .catch((err) => {
                    // eslint-disable-next-line no-console
                    console.log(err);
                });
        })
        .catch((err) => {
            // eslint-disable-next-line no-console
            console.log(err);
        });
});

// =======================> /api/authorized/post/like
router.put('/like', auth, (req, res) => {
    Post.findByIdAndUpdate(
        req.body.postId,
        {
            $push: { likes: req.userId },
        },
        {
            new: true,
        }
    )
        .populate('comments.postedBy', '-password')
        .populate('postedBy', '-password')
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err });
            } else {
                res.json(result);
            }
        });
});

// =======================> /api/authorized/post/unlike
router.put('/unlike', auth, (req, res) => {
    Post.findByIdAndUpdate(
        req.body.postId,
        {
            $pull: { likes: req.userId },
        },
        {
            new: true,
        }
    )
        .populate('comments.postedBy', '-password')
        .populate('postedBy', '-password')
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err });
            } else {
                res.json(result);
            }
        });
});

// =======================> /api/authorized/post/delete/:postid
router.delete('/delete/:postid', auth, (req, res) => {
    Post.findOne({ _id: req.params.postid })
        .populate('postedBy', '_id')
        .exec((err, post) => {
            if (err || !post) {
                return res.status(422).json({ error: err });
            }
            if (post.postedBy._id.toString() === req.userId.toString()) {
                post.remove()
                    .then((result) => {
                        res.json(result);
                    })
                    .catch((error) => {
                        // eslint-disable-next-line no-console
                        console.log(error);
                    });
            }
        });
});
// =======================> /api/authorized/post/comment
router.put('/comment', auth, (req, res) => {
    const comment = {
        text: req.body.text,
        postedBy: req.userId,
    };
    Post.findByIdAndUpdate(
        req.body.postId,
        {
            $push: { comments: comment },
        },
        {
            new: true,
        }
    )
        .populate('comments.postedBy', '-password')
        .populate('postedBy', '-password')
        .sort('-createdAt')
        .exec((err, result) => {
            if (err) {
                return res.status(422).json({ error: err });
            } else {
                res.json(result);
            }
        });
});

// =======================> /api/authorized/post/create
router.post(
    '/create',
    auth,
    multerUploads.single('img'),
    awsController.doUpload
);

module.exports = router;

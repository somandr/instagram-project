# ![alt logo insta](https://www.searchpng.com/wp-content/uploads/2018/12/Splash-Instagraam-Icon-Png-1024x1024.png) Instagram - Project

## Цель проекта:
* разработка веб-приложения, аналога [Instagram](https://www.instagram.com/)

***

#### Описание: 

Данный проект, разработан в **React.js - (MERN)**

- ***Upload фото, в проекте***, реализовано в **s3 AWS**

#### Deployment:
***Rolling Deployment:***
* **Google Cloud Platform** - Google App Engine (Dockers, flexible environment)
* <https://instagram.design-development.website>
* ***при входе в приложение, необходимо указать эл.адрес и пароль, указанный, при регистрации аккаунта***

***

### Инструкция по установке:

1. [Clone GitLab Repo](https://gitlab.com/Somandr/instagram-project)

2. npm install
* (/app)

3. npm install
* (/client)

4. npm run dev
* (/app)
* запуск проекта, в development mode

#### Pre-requisites:
- OS (Windows, Linux, Mac)
- Node.js

**Team:**
- Alex Kalyta -
    aleksandrkalyta@gmail.com
- Andrew Somin -
    a@somin.tech

***License:***
GNU GENERAL PUBLIC LICENSE




